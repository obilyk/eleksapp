//
//  UIImage+OpenCV.h
//  EleksApp
//
//  Created by Oleksii Bilyk on 9/13/16.
//  Copyright © 2016 Oleksii Bilyk. All rights reserved.
//

#import <UIKit/UIKit.h>

#ifdef __cplusplus
#import <opencv2/opencv.hpp>
#endif

@interface UIImage (OpenCV)

+ (UIImage *)imageFromCVMat:(cv::Mat)mat;

- (cv::Mat)cvMatRepresentationColor;
- (cv::Mat)cvMatRepresentationGray;

@end
