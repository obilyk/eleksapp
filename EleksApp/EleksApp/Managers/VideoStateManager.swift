//
//  VideoStateManager.swift
//  EleksApp
//
//  Created by Oleksii Bilyk on 9/23/16.
//  Copyright © 2016 Oleksii Bilyk. All rights reserved.
//

import AVFoundation

class VideoStateManager: NSObject {

    var cameraPosition: AVCaptureDevicePosition = .Front
    
    var videoFilter: EAVideoFilter?
    var maskFilter: EAMaskFilter?
    var colorFilter: EAColorFilter?
    
    var imageView: UIImageView?
    
    init(imageView: UIImageView?) {
        
        if let lImageView = imageView {
            self.imageView = lImageView
            
            self.colorFilter = EAColorFilter(cameraView: lImageView)
            self.maskFilter = EAMaskFilter(cameraView: lImageView)
            
            self.videoFilter = self.maskFilter
        }
        
        super.init()
    }
    
//MARK: public methods
    
    /**
     This method is used to start video capturing of current filter
     */
    func startVideo() {
        if let videoFilter = self.videoFilter {
            videoFilter.startCapturing()
        }
    }
    
    /**
     This method is used to stop video capturing of current filter
     */
    func stopVideo() {
        if let videoFilter = self.videoFilter {
            videoFilter.stopCapturing()
        }
    }

    /**
     This method is used to change camera position
     */
    func changeCameraPosition() {
        if let maskFilter = self.maskFilter, colorFilter = self.colorFilter {
            self.stopVideo()
            
            self.cameraPosition = self.cameraPosition == .Front ? .Back : .Front
            
            maskFilter.setCameraPosition(self.cameraPosition)
            colorFilter.setCameraPosition(self.cameraPosition)
            
            self.startVideo()
        }
    }
    
    /**
     This method is used to set new type for current filter
     
     - parameter number: another filter type
     */
    func setFilterWithNumber(number:Int!) {
        if let videoFilter = self.videoFilter {
            videoFilter.setCurrentFilterType(number)
        }
    }

    /**
     This method is used to set active mask filter type
     */
    func makeMaskFilterActive() {
        if let videoFilter = self.videoFilter {
            if videoFilter.isKindOfClass(EAMaskFilter) == false {
                self.stopVideo()
                
                self.videoFilter = self.maskFilter
                
                UIView.transitionWithView(self.imageView!,
                                          duration: 0.7,
                                          options: UIViewAnimationOptions.TransitionFlipFromRight,
                                          animations: nil,
                                          completion: {[unowned self] (finished: Bool) -> () in
                                            self.startVideo()
                                          })
            }
        }
    }
    
    /**
     This method is used to set active color filter type
     */
    func makeColorFilterActive() {
        if let videoFilter = self.videoFilter {
            if videoFilter.isKindOfClass(EAColorFilter) == false {
                self.stopVideo()
                
                self.videoFilter = self.colorFilter
                
                UIView.transitionWithView(self.imageView!,
                                          duration: 0.7,
                                          options: UIViewAnimationOptions.TransitionFlipFromLeft,
                                          animations: nil,
                                          completion: {[unowned self] (finished: Bool) -> () in
                                            self.startVideo()
                    })
            }
        }
    }
    
    /**
     This method is used to get current filter type
     
     - returns: - filter type
     */
    func currentVideoType() -> NSInteger! {
        return self.videoFilter?.videoFilterType
    }
}
