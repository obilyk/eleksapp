//
//  PreviewVC.swift
//  EleksApp
//
//  Created by Oleksii Bilyk on 9/10/16.
//  Copyright © 2016 Oleksii Bilyk. All rights reserved.
//

import UIKit

class PreviewVC: UIViewController {
    
    @IBOutlet var imageViewPreview: UIImageView!
    @IBOutlet var constraintMenuHeight: NSLayoutConstraint!
    @IBOutlet var constraintNumberViewHeight: NSLayoutConstraint!
    
    @IBOutlet var buttonsCollection: [UIButton]!
    
    var activeButtonTag: NSInteger = 0
    
    var isMenuShown: Bool = true
    var videoManager: VideoStateManager?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //inits object of class which is responsible for managing state of adding filters to video
        self.videoManager = VideoStateManager(imageView: self.imageViewPreview)
        
        //adds guesture to show/hide bottom menu
        let menuGuestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(PreviewVC.videoViewTapped(_:)))
        self.view.addGestureRecognizer(menuGuestureRecognizer)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        //start video processing
        self.videoManager?.startVideo()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        //stop video processing
        self.videoManager?.stopVideo()
    }
    
    /**
     This method is handler for guesture recognizer
     
     - parameter sender: <#sender description#>
     */
    func videoViewTapped(sender: AnyObject) {
        self.isMenuShown = !self.isMenuShown
        
        self.updateViews()
    }
    
//MARK: private methods
    /**
     This method is used show/hide menu view
     
     - parameter visible:  bool variable which define whether view should be shown/hidden
     - parameter animated: bool variable which showin/hiddin has to be animated or not
     */
    func setMenuViewVisible(visible:Bool, animated:Bool) {
        self.constraintMenuHeight.constant = visible == true ? 50 : 0
        if animated == true {
            UIView.animateWithDuration(0.25, animations: { [unowned self] in
                self.view.layoutSubviews()
            })
        }
    }
    
    
    /**
     This method is used show/hide number view
     
     - parameter visible:  bool variable which define whether view should be shown/hidden
     - parameter animated: bool variable which showin/hiddin has to be animated or not
     */
    func setNumberViewVisible(visible:Bool, animated:Bool) {
        self.constraintNumberViewHeight.constant = visible == true ? 100 : 0
        if animated == true {
            UIView.animateWithDuration(0.25, animations: { [unowned self] in
                self.view.layoutSubviews()
                })
        }
    }

    /**
     This method is used to update visibility of menu and number views
     */
    func updateViews() {
        self.setMenuViewVisible(self.isMenuShown, animated: true)
        
        if self.isMenuShown == false {
            self.setNumberViewVisible(false, animated: true)
        }
    }

    /**
     This method is used to higlight new button on number view
     
     - parameter index: index of new button which has to be selected
     */
    func selectButtonWithIndex(index: NSInteger) {
        let currentButton = self.buttonsCollection[self.activeButtonTag]
        let nextButton  = self.buttonsCollection[index]
        self.activeButtonTag = index

        UIView.animateWithDuration(0.25) { 
            currentButton.backgroundColor = UIColor.clearColor()
            nextButton.backgroundColor = UIColor.whiteColor()
        }
    }
//MARK: IBActions
    
    @IBAction func masksBtnTapped(sender: AnyObject) {
        self.setNumberViewVisible(true, animated: true)
        self.videoManager?.makeMaskFilterActive()
        
        let currentVideoType = self.videoManager?.currentVideoType()
        self.selectButtonWithIndex(currentVideoType!)
    }
    
    @IBAction func filterBtnTapped(sender: AnyObject) {
        self.setNumberViewVisible(true, animated: true)
        self.videoManager?.makeColorFilterActive()
        
        let currentVideoType = self.videoManager?.currentVideoType()
        self.selectButtonWithIndex(currentVideoType!)
    }
    
    @IBAction func changeCameraBtnTapped(sender: AnyObject) {
        self.videoManager?.changeCameraPosition()
    }
    
    @IBAction func numberViewButtonTapped(sender: AnyObject) {
        self.videoManager?.setFilterWithNumber(sender.tag)
        self.selectButtonWithIndex(sender.tag)
    }
}

