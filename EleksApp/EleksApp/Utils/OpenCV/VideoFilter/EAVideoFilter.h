//
//  EAVideoFilter.h
//  EleksApp
//
//  Created by Oleksii Bilyk on 9/23/16.
//  Copyright © 2016 Oleksii Bilyk. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <Foundation/Foundation.h>

@class CvVideoCamera;
@interface EAVideoFilter : NSObject

@property (nonatomic, strong) CvVideoCamera* videoCamera;
@property(nonatomic, assign) NSInteger videoFilterType;

- (instancetype)initWithCameraView:(UIImageView*)cameraView;

- (void)startCapturing;
- (void)stopCapturing;

- (void)setCameraPosition:(AVCaptureDevicePosition)position;
- (void)setCurrentFilterType:(NSInteger)filterType;

@end
