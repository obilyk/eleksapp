//
//  EAVideoFilter.m
//  EleksApp
//
//  Created by Oleksii Bilyk on 9/23/16.
//  Copyright © 2016 Oleksii Bilyk. All rights reserved.
//

#import "EAVideoFilter.h"

#import <opencv2/videoio/cap_ios.h>

#ifdef __cplusplus
#import <opencv2/opencv.hpp>
#endif

using namespace cv;

@interface EAVideoFilter() <CvVideoCameraDelegate>
@end

@implementation EAVideoFilter

- (instancetype)initWithCameraView:(UIImageView *)view {
    if (self = [super init]) {
        self.videoCamera = [[CvVideoCamera alloc] initWithParentView:view];
        self.videoCamera.defaultAVCaptureDevicePosition = AVCaptureDevicePositionFront;
        self.videoCamera.defaultAVCaptureSessionPreset = AVCaptureSessionPreset1280x720;
        self.videoCamera.defaultAVCaptureVideoOrientation = AVCaptureVideoOrientationPortrait;
        
        self.videoCamera.defaultFPS = 30;
        self.videoCamera.grayscaleMode = NO;
        self.videoCamera.delegate = self;
    }
    return self;
}

#pragma mark - public methods

- (void)startCapturing {
    [self.videoCamera start];
}

- (void)stopCapturing {
    [self.videoCamera stop];
}

- (void)setCameraPosition:(AVCaptureDevicePosition)position {
    self.videoCamera.defaultAVCaptureDevicePosition = position;
}

- (void)setCurrentFilterType:(NSInteger)filterType {
}

#pragma mark - CvVideoCameraDelegate

// delegate method for processing image frames
- (void)processImage:(cv::Mat&)image {
}

@end
