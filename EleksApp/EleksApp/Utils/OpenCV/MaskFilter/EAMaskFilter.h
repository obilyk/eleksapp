//
//  EAMaskFilter.h
//  EleksApp
//
//  Created by Oleksii Bilyk on 9/11/16.
//  Copyright © 2016 Oleksii Bilyk. All rights reserved.
//

#import "EAVideoFilter.h"

typedef enum {
    EAMF_1,
    EAMF_2,
    EAMF_3,
    EAMF_4
} EAMaskTypes;

@interface EAMaskFilter : EAVideoFilter
@end