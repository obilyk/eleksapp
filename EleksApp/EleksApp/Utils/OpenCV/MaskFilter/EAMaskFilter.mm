//
//  EAMaskFilter.h
//  EleksApp
//
//  Created by Oleksii Bilyk on 9/11/16.
//  Copyright © 2016 Oleksii Bilyk. All rights reserved.
//

#import "EAMaskFilter.h"
#import <opencv2/videoio/cap_ios.h>

#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"

#import <opencv2/imgcodecs/ios.h>
#include <stdio.h>

#ifdef __cplusplus
#import <opencv2/opencv.hpp>
#endif

#import "UIImage+OpenCV.h"

static const CGFloat sScaleFactor = 5.0;

@interface EAMaskFilter() <CvVideoCameraDelegate> {
    cv::CascadeClassifier _faceDetector;
    
    std::vector<cv::Rect> _faceRects;
    std::vector<cv::Mat> _faceImgs;
    
    cv::Mat _imageMask;
}
/**
 *  This method is used to set particular mask filter type
 *
 *  @param filterType - type of mask filter
 */
- (void)setMaskFilterType:(EAMaskTypes)filterType;

/**
 *  This method is used to load image which will be used as mask
 *
 *  @param type - type of mask filter
 */
- (void)loadMaskImageAndCreateMaskMatrixForType:(EAMaskTypes)type;

/**
 *  This methods is used to load HAAR file for cascade classifier
 */
- (void)loadHAARFileForCascadeClassifier;

/**
 *  This method is used to convert current image to smaller and gray image
 *  As scale factor is used sScaleFactor constant
 *
 *  @param image - original image
 *
 *  @return smaller and gray variant of image which is passed as parametr of function
 */
- (cv::Mat)makeImageSmallAndGray:(cv::Mat &)image;

/**
 *  This method is used to add mask on image
 * 
 *  @param image - original image
 */
- (void)addMaskToImage:(cv::Mat&)image;



@property (nonatomic, assign) CGFloat scale;

@end

@implementation EAMaskFilter

- (instancetype)initWithCameraView:(UIImageView *)cameraView {
    if (self = [super initWithCameraView:cameraView]) {
        
        [self loadMaskImageAndCreateMaskMatrixForType:EAMF_1];
        
        [self loadHAARFileForCascadeClassifier];
    }
    return self;
}

#pragma mark - CvVideoCameraDelegate

// delegate method for processing image frames
- (void)processImage:(cv::Mat &)image {
    //get small and gray version of image
    cv::Mat smallImg = [self makeImageSmallAndGray:image];
  
    //detect faces on small gray image
    self->_faceDetector.detectMultiScale(smallImg,
                                         self->_faceRects,
                                         1.1,
                                         2,
                                         0|CV_HAAR_SCALE_IMAGE,
                                         cv::Size(30,30)
                                         );
    
    //add mask to image
    [self addMaskToImage:image];
}

#pragma mark - private methods

/**
 *  This method is used to set particular mask filter type
 *
 *  @param filterType - type of mask filter
 */
- (void)setMaskFilterType:(EAMaskTypes)filterType {
    self.videoFilterType = filterType;
    
    [self loadMaskImageAndCreateMaskMatrixForType:filterType];
}

/**
 *  This method is used to load image which will be used as mask
 */
- (void)loadMaskImageAndCreateMaskMatrixForType:(EAMaskTypes)type {
    NSString* lImagName = [NSString stringWithFormat:@"mask_%d", (int)type];
    UIImage* lResImage = [UIImage imageNamed:lImagName];
    
    _imageMask = [lResImage cvMatRepresentationColor];
}

/**
 *  This methods is used to load HAAR file for cascade classifier
 */
- (void)loadHAARFileForCascadeClassifier {
    NSString *faceCascadePath = [[NSBundle mainBundle] pathForResource:@"haarcascade_frontalface_alt2" ofType:@"xml"];
    
    const CFIndex CASCADE_NAME_LEN = 2048;
    char *CASCADE_NAME = (char *) malloc(CASCADE_NAME_LEN);
    
    CFStringGetFileSystemRepresentation( (CFStringRef)faceCascadePath, CASCADE_NAME, CASCADE_NAME_LEN);
    
    if(_faceDetector.load(CASCADE_NAME) == YES) {
        NSLog(@"HAAR file for cascade classifier was loaded succesfully!");
    } else {
        NSLog(@"Failed to load HAAR file for cascade classifier!");
    }
    
    free(CASCADE_NAME);
}

/**
 *  This method is used to convert current image to smaller and gray image
 *  As scale factor is used sScaleFactor constant
 *
 *  @param image - original image
 *
 *  @return smaller and gray variant of image which is passed as parametr of function
 */
- (cv::Mat)makeImageSmallAndGray:(cv::Mat &)image {
    cv::Mat grayImage, smallImg(cvRound(image.rows/sScaleFactor), cvRound(image.cols/sScaleFactor), CV_8UC1);
    
    //convert current to gray image
    cvtColor(image, grayImage, cv::COLOR_BGR2GRAY);
    
    //resize gray image to small image size
    resize(grayImage, smallImg, smallImg.size(), 0, 0, cv::INTER_LINEAR);
    
    equalizeHist(smallImg, smallImg);
    
    return smallImg;
}

/**
 *  This method is used to add mask on image
 *
 *  @param image - original image
 */
- (void)addMaskToImage:(cv::Mat&)image {
    for(std::vector<cv::Rect>::const_iterator r = _faceRects.begin(); r != _faceRects.end(); r++) {
        cv::Mat mask1, src1;
        
        //gets original face size
        cv::Size faceSize(r->width*sScaleFactor, r->height*sScaleFactor);
        
        //resize mask to face size
        resize(_imageMask, mask1, faceSize);
        
        cv::Rect roi(r->x*sScaleFactor, r->y*sScaleFactor, faceSize.width, faceSize.height);
        
        image(roi).copyTo(src1);
        
        resize(src1, src1, faceSize);
        
        cv::Mat mask2, m, m1, inv_mask;
        
        //create gray mask
        cvtColor(mask1, mask2, CV_BGR2GRAY);
        threshold(mask2, mask2, 10, 255, CV_THRESH_BINARY);
        
        bitwise_not(mask2, inv_mask);
        
        std::vector<cv::Mat> maskChannels(4), result_mask(4);
        
        //create mask which will be added to ROI
        split(mask1, maskChannels);
        
        bitwise_and(maskChannels[0],mask2,result_mask[0]);
        bitwise_and(maskChannels[1],mask2,result_mask[1]);
        bitwise_and(maskChannels[2],mask2,result_mask[2]);
        bitwise_and(maskChannels[3],mask2,result_mask[3]);
        
        merge(result_mask, m);
        
        std::vector<cv::Mat> srcChannels(4);
        
        //prepare ROI for merging with mask
        split(src1, srcChannels);
        
        bitwise_and(srcChannels[0],inv_mask,result_mask[0]);
        bitwise_and(srcChannels[1],inv_mask,result_mask[1]);
        bitwise_and(srcChannels[2],inv_mask,result_mask[2]);
        bitwise_and(srcChannels[3],inv_mask,result_mask[3]);
        
        merge(result_mask, m1);
        
        //merge mask with ROI
        add(m, m1, mask1);
        
        mask1.copyTo(image(roi));
    }
}

#pragma mark - public methods

- (void)startCapturing {
    [self.videoCamera start];
}

- (void)stopCapturing {
    [self.videoCamera stop];
}

- (void)setCurrentFilterType:(NSInteger)filterType {
    [self setMaskFilterType:(EAMaskTypes)filterType];
}



@end
