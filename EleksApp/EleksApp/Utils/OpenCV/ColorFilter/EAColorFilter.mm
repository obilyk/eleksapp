//
//  EAColorFilter.h
//  EleksApp
//
//  Created by Oleksii Bilyk on 9/11/16.
//  Copyright © 2016 Oleksii Bilyk. All rights reserved.
//

#import "EAColorFilter.h"
#import <opencv2/videoio/cap_ios.h>

#ifdef __cplusplus
#import <opencv2/opencv.hpp>
#endif

using namespace cv;

@interface EAColorFilter() <CvVideoCameraDelegate> {
    ColorConversionCodes _BGR2Filter;
    ColorConversionCodes _filter2BRG;
}

/**
 *  This method is used to change color filter type
 *
 *  @param filterType - type of color filter
 */
- (void)setColorFilterType:(EAColorTypes)filterType;

@end

@implementation EAColorFilter

- (instancetype)initWithCameraView:(UIImageView*)cameraView {
    if (self = [super initWithCameraView:cameraView]) {
  
        [self setColorFilterType:EACT_Gray];
    }
    return self;
}

#pragma mark - public methods

- (void)startCapturing {
    [self.videoCamera start];
}

- (void)stopCapturing {
    [self.videoCamera stop];
}

- (void)setCurrentFilterType:(NSInteger)filterType {
    [self stopCapturing];
    
    [self setColorFilterType:(EAColorTypes)filterType];
    
    [self startCapturing];
}

#pragma mark - CvVideoCameraDelegate

// delegate method for processing image frames
- (void)processImage:(Mat&)image {
    Mat image_copy;
    cvtColor(image, image_copy, _BGR2Filter);
    
    bitwise_not(image_copy, image_copy);
    
    Mat bgr;
    cvtColor(image_copy, bgr, _filter2BRG);
    
    cvtColor(bgr, image, COLOR_BGR2BGRA);
}

#pragma mark - private methods

/**
 *  This method is used to change color filter type
 *
 *  @param filterType - type of color filter
 */
- (void)setColorFilterType:(EAColorTypes)filterType {
    self.videoFilterType = filterType;
    
    switch (filterType) {
        case EACT_Gray:
            
            _filter2BRG = COLOR_GRAY2BGR;
            _BGR2Filter = COLOR_BGR2GRAY;
            
            break;
            
        case EACT_HSV:
            
            _filter2BRG = COLOR_HSV2BGR;
            _BGR2Filter = COLOR_BGR2HSV;
            
            break;
            
        case EACT_XYZ:
            
            _filter2BRG = COLOR_XYZ2BGR;
            _BGR2Filter = COLOR_BGR2XYZ;
            
            break;
            
        case EACT_Lab:
            
            _filter2BRG = COLOR_Lab2BGR;
            _BGR2Filter = COLOR_BGR2Lab;
            
            break;
    }
}

@end
