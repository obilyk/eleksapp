//
//  EAColorFilter.h
//  EleksApp
//
//  Created by Oleksii Bilyk on 9/11/16.
//  Copyright © 2016 Oleksii Bilyk. All rights reserved.
//

#import "EAVideoFilter.h"

typedef enum {
    EACT_Gray,
    EACT_HSV,
    EACT_XYZ,
    EACT_Lab
} EAColorTypes;

@interface EAColorFilter : EAVideoFilter
@end
